# TATVA SOFT PRACTCLE

## Installation

clone repository

```bash
git clone https://gitlab.com/dabhekarankit4/tatvasoft-practicle.git
```

install node modules

```
npm i
```

copy .env.example to .nev

```
cp .env.example .env
```

set all below details in .env

```
APP_NAME={your app name}
APP_KEY={unique string}
APP_URL=http://localhost:3000
PORT=3000

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_USER={database user name}
DB_PASSWORD={database user password}
DB_NAME={database name}
```

run migration

```
npx knex migrate:latest
```

Run project

```
npm run start
```

open in browser

```
http://localhost:3000/api/documentation
```

## Used NPM Package

### knex, mysql

database connection

### bcrypt

For encrypt string

### dotenv

To access env variable

### joi, express-joi-validation

For validation

### jsonwebtoken

for user authentication

### swagger-ui-express

for request list
