import dotenv from "dotenv";
dotenv.config();
import jwt from "jsonwebtoken";
import knex from "../common/config/db.config";
import {
  compareHashString,
  encryptString,
} from "../common/helper/bcrypt.helper";

class AuthService {
  /**
   * register user
   * @param {object} body
   */
  static async register(body) {
    const checkUser = await this.findByEmail(body.email);
    if (checkUser) {
      throw new Error("This email is already register with us.");
    }

    const password = await encryptString(body.password);
    const [userId] = await knex("users").insert({ ...body, password });
    const user = await this.findById(userId);

    const accessToken = jwt.sign(
      { sub: user.id },
      process.env.APP_KEY || "sdafewfeac"
    );

    return {
      ...user,
      auth: {
        accessToken,
      },
    };
  }

  /**
   * login user
   * @param {object} body
   */
  static async login(body) {
    const user = await this.findByEmail(body.email);
    if (!user) {
      throw new Error("This email is not register with us.");
    }

    const checkPassword = await compareHashString(body.password, user.password);
    if (!checkPassword) {
      throw new Error("Invalid credentials.");
    }

    const accessToken = jwt.sign(
      { sub: user.id },
      process.env.APP_KEY || "sdafewfeac"
    );

    return {
      ...user,
      auth: {
        accessToken,
      },
    };
  }

  /**
   * find user by id
   * @param {integer} userId
   * @returns
   */
  static async findById(userId) {
    return await knex("users").where("id", "=", userId).first();
  }

  /**
   * find user by email
   * @param {string} email
   * @returns
   */
  static async findByEmail(email) {
    return await knex("users").where("email", "=", email).first();
  }
}

export default AuthService;
