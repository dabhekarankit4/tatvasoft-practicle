import express from "express";
import AuthController from "./auth.controller";
import validator from "../common/config/validator.config";
import registerDto from "./dtos/register.dto";
import expressAsyncHandler from "express-async-handler";
import loginDto from "./dtos/login.dto";

const router = express.Router();

router.post(
  "/register",
  validator.body(registerDto),
  expressAsyncHandler(AuthController.register)
);

router.post(
  "/login",
  validator.body(loginDto),
  expressAsyncHandler(AuthController.login)
);

export default router;
