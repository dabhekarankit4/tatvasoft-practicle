import Joi from "joi";

export default Joi.object({
  firstName: Joi.string().min(3).max(30).required(),
  lastName: Joi.string().min(3).max(30).required(),
  email: Joi.string().email().min(3).max(30).required(),
  password: Joi.string().min(6).max(10).required(),
  dob: Joi.date().less("now").required(),
  role: Joi.string().valid("admin", "user").required(),
});
