import Joi from "joi";

export default Joi.object({
  email: Joi.string().email().min(3).max(30).required(),
  password: Joi.string().min(6).max(10).required(),
});
