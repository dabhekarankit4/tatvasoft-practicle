import AuthService from "./auth.service";

class AuthController {
  /**
   * regoster user
   * @param {object} req
   * @param {object} res
   */
  static async register(req, res) {
    const data = await AuthService.register(req.body);
    res.json({ data });
  }

  /**
   * login user
   * @param {object} req
   * @param {object} res
   */
  static async login(req, res) {
    const data = await AuthService.login(req.body);
    res.json({ data });
  }
}

export default AuthController;
