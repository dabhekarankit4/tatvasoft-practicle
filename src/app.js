import dotenv from "dotenv";
dotenv.config();
import express from "express";
import errorMiddleware from "./common/middleware/error.middleware";
import apiRouter from "./router/api.routes";
import swaggerUi from "swagger-ui-express";
import YAML from "yamljs";
const swaggerDocument = YAML.load("./swagger.yml");

const app = express();

app.use(express.json());

app.use("/api/v1", apiRouter);
app.use(
  "/api/documentation",
  swaggerUi.serve,
  swaggerUi.setup(swaggerDocument)
);

app.use(errorMiddleware);

app.listen(process.env.PORT, () => {
  console.log("Server runing on:", process.env.PORT);
});
