import express from "express";
import authRoutes from "../auth/auth.routes";
import blogRoutes from "../blogs/blogs.routes";
import authenticationMiddleware from "../common/middleware/authentication.middleware";

const router = express.Router();

router.use("/auth", authRoutes);
router.use("/blogs", authenticationMiddleware, blogRoutes);

export default router;
