export default (err, req, res, next) => {
  // console.log(err.error.isJoi);
  if (err && err.error && err.error.isJoi) {
    return res.status(422).json({
      status: "error",
      message: err.error.details[0].message,
    });
  } else {
    console.log(err);
    return res.status(404).json({
      status: "error",
      message: err.message,
    });
  }
};
