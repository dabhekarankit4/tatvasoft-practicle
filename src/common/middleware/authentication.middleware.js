import dotenv from "dotenv";
dotenv.config();
import jwt from "jsonwebtoken";
import knex from "../config/db.config";

export default async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, process.env.APP_KEY || "sdafewfeac");
    req.user = await knex("users").where("id", decoded.sub).first();
    next();
  } catch (error) {
    return res.status(401).json({ message: "unauthorized" });
  }
};
