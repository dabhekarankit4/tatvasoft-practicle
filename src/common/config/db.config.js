import knex from "knex";
import { attachPaginate } from "knex-paginate";
import knexfile from "../../../knexfile";

attachPaginate();
export default knex(knexfile);
