import bcrypt from "bcrypt";

const saltRounds = 10;

export const encryptString = async (string) => {
  const salt = await bcrypt.genSalt(saltRounds);

  return bcrypt.hash(string, salt);
};

export const compareHashString = async (string, hashString) => {
  return bcrypt.compare(string, hashString);
};
