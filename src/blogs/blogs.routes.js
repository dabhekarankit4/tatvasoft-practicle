import express from "express";
import validator from "../common/config/validator.config";
import expressAsyncHandler from "express-async-handler";
import BlogController from "./blogs.controller";
import createBlogDto from "./dtos/create-blog.dto";

const router = express.Router();

router.get("/", expressAsyncHandler(BlogController.index));

router.post(
  "/",
  validator.body(createBlogDto),
  expressAsyncHandler(BlogController.store)
);

router.put(
  "/:blog",
  validator.body(createBlogDto),
  expressAsyncHandler(BlogController.update)
);

router.delete("/:blog", expressAsyncHandler(BlogController.delete));

export default router;
