import BlogService from "./blogs.service";

class BlogController {
  /**
   * get Blog list
   * @param {*} req
   * @param {*} res
   */
  static async index(req, res) {
    const data = await BlogService.index(req.user, req.query);
    return res.json({ data });
  }

  /**
   * store Blog
   * @param {*} req
   * @param {*} res
   */
  static async store(req, res) {
    const data = await BlogService.store(req.body, req.user);
    return res.json({ data });
  }

  /**
   * update Blog
   * @param {*} req
   * @param {*} res
   */
  static async update(req, res) {
    const data = await BlogService.update(req.body, req.params.blog, req.user);
    return res.json({ data });
  }

  /**
   * delete Blog
   * @param {*} req
   * @param {*} res
   */
  static async delete(req, res) {
    await BlogService.delete(req.params.blog, req.user);
    return res.json({ message: "Blog deleted successfully." });
  }
}

export default BlogController;
