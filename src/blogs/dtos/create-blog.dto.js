import Joi from "joi";

export default Joi.object({
  title: Joi.string().min(3).max(30).required(),
  description: Joi.string().required(),
  publisedAt: Joi.date().less("now").required(),
  status: Joi.string().valid("publish", "unpublish").required(),
  author: Joi.string().min(3).max(30).required(),
  categoryId: Joi.number().required(),
});
