import moment from "moment";
import knex from "../common/config/db.config";

class BlogService {
  /**
   * get Blog list
   * @param {object} authUser
   */
  static async index(authUser, query) {
    const { perPage, currentPage, publisedAt, modifiedAt, category, author } =
      query;
    const qb = knex("blogs");
    if (authUser.role !== "admin") {
      qb.where("userId", "=", authUser.id);
    }
    if (publisedAt) {
      qb.where(knex.raw("date(publisedAt)"), "=", publisedAt);
    }
    if (modifiedAt) {
      qb.where(knex.raw("date(modifiedAt)"), "=", modifiedAt);
    }
    if (category) {
      qb.where("categoryId", "=", category);
    }
    if (author) {
      qb.where("author", "LIKE", `%${author}%`);
    }
    return await qb.paginate({ perPage, currentPage });
  }

  /**
   * store Blog
   * @param {*} body
   * @param {*} authUser
   */
  static async store(body, authUser) {
    const category = await knex("categories")
      .where("id", "=", body.categoryId)
      .first();

    if (!category) {
      throw new Error("category not found.");
    }

    const [blogId] = await knex("blogs").insert({
      ...body,
      userId: authUser.id,
    });

    return await this.findById(blogId);
  }

  /**
   * find blog by id
   * @param {integer} blogId
   * @returns
   */
  static async findById(blogId) {
    return await knex("blogs").where("id", "=", blogId).first();
  }

  /**
   * update Blog
   * @param {*} body
   * @param {*} blogId
   */
  static async update(body, blogId, authUser) {
    const blog = await this.findById(blogId);
    if (!blog) {
      throw new Error("Blog not found.");
    }

    if (authUser.role !== "admin") {
      if (authUser.id !== blog.userId) {
        throw new Error("You can't update this blog.");
      }
    }

    const category = await knex("categories")
      .where("id", "=", body.categoryId)
      .first();
    if (!category) {
      throw new Error("category not found.");
    }

    await knex("blogs")
      .where("id", "=", blogId)
      .update({
        ...body,
        modifiedAt: moment().utc().format("YYYY-MM-DD"),
      });

    return await this.findById(blogId);
  }

  /**
   * delete Blog
   * @param {*} req
   * @param {*} res
   */
  static async delete(blogId, authUser) {
    const blog = await this.findById(blogId);
    if (!blog) {
      throw new Error("Blog not found.");
    }

    if (authUser.role !== "admin") {
      if (authUser.id !== blog.userId) {
        throw new Error("You can't delete this blog.");
      }
    }

    return await knex("blogs").where("id", "=", blogId).delete();
  }
}

export default BlogService;
