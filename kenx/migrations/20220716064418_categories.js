/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema
    .createTable("categories", function (table) {
      table.increments("id");
      table.string("name", 255).notNullable();
      table.timestamp("createdAt").defaultTo(knex.fn.now());
      table.timestamp("updatedAt").defaultTo(knex.fn.now());
    })
    .then(async () => {
      await knex("categories").insert([
        {
          name: "cat 1",
        },
        {
          name: "cat 2",
        },
        {
          name: "cat 3",
        },
      ]);
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("categories");
};
