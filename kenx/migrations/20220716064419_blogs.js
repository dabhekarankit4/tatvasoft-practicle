/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("blogs", function (table) {
    table.increments("id");
    table.integer("userId").references("users.id").unsigned();
    table.integer("categoryId").references("categories.id").unsigned();
    table.string("title", 255).notNullable();
    table.text("description").notNullable();
    table.timestamp("publisedAt").notNullable();
    table.timestamp("modifiedAt");
    table.enum("status", ["publish", "unpublish"]);
    table.string("author", 255).notNullable();
    table.timestamp("createdAt").defaultTo(knex.fn.now());
    table.timestamp("updatedAt").defaultTo(knex.fn.now());
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("blogs");
};
